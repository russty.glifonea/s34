const express = require('express')
const app = express()
const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.listen(port, () => console.log(`Server is running at localhost:${port}`))

let users = []

app.get("/home", (request, response) =>{
	response.send('Welcome to the home page!')
})

app.get("/user", (request, response) => {
	if(request.body.username !== '' && request.body.password !== '')
	{
		users.push(request.body)
		response.send(`User ${request.body.username} successfully registered!`)
	}
	else
	{
		response.send('Please input BOTH username and password!')
	}
})

app.post("/users", (request, response) => {
	if(request.body.username !== '' && request.body.password !== '')
	{
		users.push(request.body)
		response.send(`User ${request.body.username} successfully registered!`)
	}
	else
	{
		response.send('Please input BOTH username and password!')
	}
})

app.get("/delete-user", (request, response) => {
	
	let message = "USer does not exist!";

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice(i, 1);
			message = `User ${request.body.username} has been delete`

			break;
		}
	}
	response.send(message);
})